﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicalProgrammingLanguage
{

    /// <summary>
    /// interface created.
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// draws the shape
        /// </summary>
        /// <param name="g"></param>
        void draw(Graphics g);

        /// <summary>
        /// sets the certain parameter.
        /// </summary>
        /// <param name="list"></param>
        void set(params int[] list);
    }
}
