﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguage
{
   
    abstract class Creator //it is used to create an object
    {
        
        public abstract IShape getShape(string ShapeType);//this method is used to shape an object

    }
}
